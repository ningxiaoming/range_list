require_relative "test_base"

class TestRangeList < Minitest::Test
  def setup
    @range_list = RangeList.new
    @not_impl_hash = NotImplHash.new
  end

  def test_add_validate_param
    assert_raises(ArgumentError) { @range_list.add(nil) }
    assert_raises(ArgumentError) { @range_list.add('') }
    assert_raises(ArgumentError) { @range_list.add([1]) }
    assert_raises(ArgumentError) { @range_list.add(['1', 1]) }
    assert_raises(ArgumentError) { @range_list.add([1, '1']) }
    assert_raises(ArgumentError) { @range_list.add([3, 1]) }
  end

  def test_del_validate_param
    assert_raises(ArgumentError) { @range_list.remove(nil) }
    assert_raises(ArgumentError) { @range_list.remove('') }
    assert_raises(ArgumentError) { @range_list.remove([1]) }
    assert_raises(ArgumentError) { @range_list.remove(['1', 1]) }
    assert_raises(ArgumentError) { @range_list.remove([1, '1']) }
    assert_raises(ArgumentError) { @range_list.remove([3, 1]) }
  end

  def test_standard_case
    @range_list.add([1, 5])
    assert_output("[1, 5)\n") { @range_list.print } ## Should display: [1, 5)

    @range_list.add([10, 20])
    assert_output("[1, 5) [10, 20)\n") { @range_list.print } ## Should display: [1, 5) [10, 20)

    @range_list.add([20, 20])
    assert_output("[1, 5) [10, 20)\n") { @range_list.print } ## Should display: [1, 5) [10, 20)

    @range_list.add([20, 21])
    assert_output("[1, 5) [10, 21)\n") { @range_list.print } ## Should display: [1, 5) [10, 21)

    @range_list.add([2, 4])
    assert_output("[1, 5) [10, 21)\n") { @range_list.print } ## Should display: [1, 5) [10, 21)

    @range_list.add([3, 8])
    assert_output("[1, 8) [10, 21)\n") { @range_list.print } ## Should display: [1, 8) [10, 21)

    @range_list.remove([10, 10])
    assert_output("[1, 8) [10, 21)\n") { @range_list.print } ## Should display: [1, 8) [10, 21)

    @range_list.remove([10, 11])
    assert_output("[1, 8) [11, 21)\n") { @range_list.print } ## Should display: [1, 8) [11, 21)

    @range_list.remove([15, 17])
    assert_output("[1, 8) [11, 15) [17, 21)\n") { @range_list.print } ## Should display: [1, 8) [11, 15) [17, 21)

    @range_list.remove([3, 19])
    assert_output("[1, 3) [19, 21)\n") { @range_list.print } ## Should display: [1, 3) [19, 21)
  end

  def test_range_list
    assert_nil @range_list.add([1, 1])
    @range_list.add([2, 5])
    assert_output("[2, 5)\n") { @range_list.print }
    assert_nil @range_list.add([3, 4])
    assert_output("[2, 5)\n") { @range_list.print }
    @range_list.add([3, 7])
    assert_output("[2, 7)\n") { @range_list.print }
    @range_list.add([1, 3])
    assert_output("[1, 7)\n") { @range_list.print }
    @range_list.add([8, 10])
    assert_output("[1, 7) [8, 10)\n") { @range_list.print }

    assert_nil @range_list.remove([1, 1])
    @range_list.remove([1, 8])
    assert_output("[8, 10)\n") { @range_list.print }
    assert_nil @range_list.remove([2, 6])
    @range_list.add([3, 8])
    assert_output("[3, 10)\n") {@range_list.print}
    @range_list.remove([4, 6])
    assert_output("[3, 4) [6, 10)\n") {@range_list.print}
    assert_output("{3=>4, 6=>10}\n"){p @range_list.query_all}
  end

  def test_not_impl_hash
    assert_raises(NotImplementedError) { @not_impl_hash.put(1, 3) }
    assert_raises(NotImplementedError) { @not_impl_hash.floor_range(1) }
    assert_raises(NotImplementedError) { @not_impl_hash.delete(1) }
    assert_raises(NotImplementedError) { @not_impl_hash.each { |e| p e } }
  end

end