
require "simplecov"
SimpleCov.start do
  add_filter "vendor"
end

require "range_list"
require 'range_list/infrastructure/not_impl_hash'

require "minitest/autorun"
