# frozen_string_literal: true

require_relative 'range_list/version'
require_relative 'range_list/infrastructure/red_black_tree_impl'
# RangeList is a quick Range Query Library
class RangeList

  def initialize
    @red_black_tree_impl = RedBlackTreeImpl.new
  end

  # Add range to rangeList
  # @param [Array<Integer>] range is a range.range.length must be 2，the range[0] is range start,range[1] is range end
  # range[0] must be less than or equal to range[0]
  # @return nil
  # @raise ArgumentError
  def add(range)
    validate_param(range)
    return if range[0] == range[1]

    start_floor_range = @red_black_tree_impl.floor_range(range[0])
    return if !start_floor_range.nil? && start_floor_range[1] >= range[1]

    # find start
    start = if !start_floor_range.nil? && start_floor_range[1] >= range[0]
              start_floor_range[0]
            else
              range[0]
            end
    # find last
    last_floor_range = @red_black_tree_impl.floor_range(range[1])
    last = if !last_floor_range.nil? && last_floor_range[1] >= range[1]
             last_floor_range[1]
           else
             range[1]
           end
    # delete old range
    @red_black_tree_impl.sub_hash_key(start + 1, last).each do |k|
      @red_black_tree_impl.delete(k)
    end
    # add new range
    @red_black_tree_impl.put(start, last)
    nil
  end

  # Remove range to rangeList
  # @param [Array<Integer>] range is a range. range length must be 2，the range[0] is range start,range[1] is range end
  # range[0] must be less than or equal to range[0]
  # @return nil
  # @raise ArgumentError
  def remove(range)
    validate_param(range)
    return if range[0] == range[1]

    #  find add add ranges greater than range[0]
    end_floor_range = @red_black_tree_impl.floor_range(range[1] - 1)
    return if end_floor_range.nil?

    @red_black_tree_impl.put(range[1], end_floor_range[1]) if !end_floor_range.nil? && end_floor_range[1] > range[1]

    #  find add add ranges less than range[0]
    start_floor_range = @red_black_tree_impl.floor_range(range[0] - 1)
    @red_black_tree_impl.put(start_floor_range[0], range[0]) if !start_floor_range.nil? && start_floor_range[1] > range[0]

    # delete range
    @red_black_tree_impl.sub_hash_key(range[0], range[1] - 1).each do |k|
      @red_black_tree_impl.delete(k)
    end
    nil
  end

  def print
    item = ''
    # splicing range
    @red_black_tree_impl.each do |k, v|
      item += "[#{k}, #{v}) "
    end
    puts item[0, item.length - 1]
  end

  def query_all
    res_hash = {}
    @red_black_tree_impl.each do |k,v|
      res_hash[k] = v
    end
    res_hash
  end

  private

  def validate_param(range)
    raise ArgumentError, 'range cannot be nil' if range.nil?
    raise ArgumentError, 'range should be an array' unless range.is_a?(Array)
    raise ArgumentError, 'the length of the range should be 2' if !range.length == 2
    raise ArgumentError, 'range[0] is not a number' unless range[0].is_a?(Integer)
    raise ArgumentError, 'range[1] is not a number' unless range[1].is_a?(Integer)
    raise ArgumentError, 'range[0] should be less than or equal to range[1]' if range[0] > range[1]
  end
end