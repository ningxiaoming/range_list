require_relative 'i_hash'
require 'rbtree'


class RedBlackTreeImpl
  include IHash

  def initialize
    @rbtree = RBTree.new
  end

  def put(key, value)
    @rbtree[key] = value
  end

  def each(&block)
    @rbtree.each(&block)
  end

  # Gets the entry corresponding to the specified key; if no such entry
  # exists, returns the entry for the greatest key less than the specified
  # key; if no such entry exists, returns {@code nil}.
  def floor_range(key)
    @rbtree.upper_bound(key)
  end

  def delete(key)
    @rbtree.delete(key)
  end

  def sub_hash_key(start_key, end_key)
    keys = []
    @rbtree.bound(start_key, end_key).to_a.each do |k, v|
      keys.push k
    end
    keys
  end
end
