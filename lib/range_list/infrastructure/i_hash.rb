module IHash
  def put(key, value)
    raise NotImplementedError, "#{self.class} not implemented #{__method__} method"
  end

  def each(&block)
    raise NotImplementedError, "#{self.class} not implemented #{__method__} method"
  end

  # Gets the entry corresponding to the specified key; if no such entry
  # exists, returns the entry for the greatest key less than the specified
  # key; if no such entry exists, returns {@code nil}.
  def floor_range(key)
    raise NotImplementedError, "#{self.class} not implemented #{__method__} method"
  end

  def delete(key)
    raise NotImplementedError, "#{self.class} not implemented #{__method__} method"
  end
end
